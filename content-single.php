<?php
/**
 * Content For Single Audio
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?> role="article" itemscope="" itemprop="blogPost" itemtype="http://schema.org/BlogPosting">
	<h1 class="page-title title" itemprop="name">
		<?php the_title(); ?>
	</h1>

    <?php tatada_entry_meta() ?>

    <div class="post-content"><?php the_content() ?></div>
	<div class="tags">
		<?php echo get_the_tag_list('<i class="fa fa-tags"></i> Tags: ', ', ', '') ?>
	</div>

	<?php tatada_comment_facebook( get_the_permalink() ) ?>

	<div class="related-posts">
        <h4 class="title">Bài viết liên quan</h4>
		<div class="">
			<div class="blog-posts posts-standard">

				<?php tatada_related_posts( get_the_ID() ) ?>

			</div>
		</div>
	</div>
</article><!-- #post-## -->