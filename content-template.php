<?php
/**
 * Content For Template
 */
?>

<div class="col-md-4 col-sm-6 col-xs-12">
    <div class="wrap">
        <div class="relative">

            <?php the_post_thumbnail( 'full' ) ?>

            <div class="hover"></div>
            <div class="action">
                <a class="button demo" href="<?php echo home_url( '/demo/?template_id=' . get_the_ID() ) ?>" target="_blank">Xem Demo</a>
                <a class="button create-website" href="#" data-toggle="modal" data-target="#create-website-modal" data-id="<?php the_ID() ?>">Tạo Website</a>
            </div>
        </div>
        <h4 class="template-title"><?php the_title() ?></h4>
    </div>
</div>