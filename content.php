<?php
/**
 * Default Content
 */
?>

<article <?php post_class( 'clearfix' ) ?> role="article" itemscope="" itemprop="blogPost" itemtype="http://schema.org/BlogPosting">

	<?php tatada_thumbnail('post-thumbnail') ?>

	<h3 class="post-title title" itemprop="name">
		<a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title_attribute(); ?>">
			<?php the_title(); ?>
		</a>
	</h3>

	<?php tatada_entry_meta() ?>

	<div class="post-excerpt"><?php the_excerpt() ?></div>

</article>