<?php

/**
 * Assign the Tatada version to a var
 */
$theme = wp_get_theme( 'tatada' );
$theme_version = $theme['Version'];

add_action( 'after_setup_theme', 'tatada_setup' );

if ( !function_exists( 'tatada_setup' ) )
{
    function tatada_setup()
    {
        // wp-content/themes/tatada/languages/it_IT.mo
        load_theme_textdomain( 'tatada', get_template_directory() . '/languages');

        /**
         * Add default posts and comments RSS feed links to head.
         */
        add_theme_support( 'automatic-feed-links' );

        /**
         * Enable support for Post Thumbnails on posts and pages.
         */
        add_theme_support( 'post-thumbnails' );

        // Register Nav Menus
        register_nav_menus( array(
            'main-nav' => __( 'Main Nav', 'tatada' ),
            'mobile-nav' => __( 'Mobile Nav', 'tatada' )
        ));

        /*
         * Switch default core markup for search form, comment form, comments, galleries, captions and widgets
         * to output valid HTML5.
         */
        add_theme_support( 'html5', array(
            'search-form',
            'comment-form',
            'comment-list',
            'gallery',
            'caption',
            'widgets',
        ));

        // Declare support for title theme feature
        add_theme_support( 'title-tag' );

        // add image size
        set_post_thumbnail_size( 250, 155, true );
        //add_image_size( 'slider', 848, 350, true );
    }
}
// end function tatada_setup


/**
 * Create custom taxonomy and custom post type
 */
//add_action( 'init', 'tatada_create_custom_taxonomies' );
add_action( 'init', 'tatada_create_custom_post_types' );

function tatada_create_custom_taxonomies()
{
    register_taxonomy('audio-category', 'audio', array(
        'labels' => array(
            'name' => 'Audio Categories',
            'singular' => 'Audio Category',
            'menu_name' => 'Audio Categories'
        ),
        'hierarchical' => true,
        'public' => true,
        'show_ui' => true,
        'show_admin_column' => true,
        'show_in_nav_menus' => true,
        'show_tagcloud' => true,
        'rewrite' => array(
            'slug' => ''
        )
    ));
}

function tatada_create_custom_post_types()
{
    register_post_type( 'template', array(
        'labels' => array(
            'name' => 'Templates',
            'singular_name' => 'Template'
        ),
        'description' => 'Template to create website',
        'supports' => array(
            'title',
            'thumbnail'
        ),
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'show_in_nav_menus' => true,
        'show_in_admin_bar' => true,
        'menu_position' => 7,
        'menu_icon' => 'dashicons-images-alt2',
        'can_export' => true,
        'has_archive' => false,
        'exclude_from_search' => true,
        'publicly_queryable' => true,
        'capability_type' => 'post'
    ));

    register_post_type( 'testimonial', array(
        'labels' => array(
            'name' => 'Testimonials',
            'singular_name' => 'Testimonial'
        ),
        'description' => 'Testimonials',
        'supports' => array(
            'title',
            'editor',
            'thumbnail'
        ),
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'show_in_nav_menus' => false,
        'show_in_admin_bar' => true,
        'menu_position' => 7,
        'menu_icon' => 'dashicons-smiley',
        'can_export' => true,
        'has_archive' => false,
        'exclude_from_search' => true,
        'publicly_queryable' => true,
        'capability_type' => 'post'
    ));
}

/**
 * Register widget area
 */
add_action( 'widgets_init', 'tatada_widgets_init' );

function tatada_widgets_init()
{
    register_sidebar( array(
        'name' => __( 'Main Sidebar', 'tatada' ),
        'id' => 'sidebar-main',
        'description' => '',
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget' => '</aside>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ));

    register_sidebar( array(
        'name' => __( 'Footer Sidebar 1', 'tatada' ),
        'id' => 'sidebar-footer-1',
        'description' => '',
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget' => '</aside>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ));

    register_sidebar( array(
        'name' => __('Footer Sidebar 2', 'tatada' ),
        'id' => 'sidebar-footer-2',
        'description' => '',
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget' => '</aside>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ));

    register_sidebar( array(
        'name' => __( 'Footer Sidebar 3', 'tatada' ),
        'id' => 'sidebar-footer-3',
        'description' => '',
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget' => '</aside>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ));

    register_sidebar( array(
        'name' => __( 'Footer Sidebar 4', 'tatada' ),
        'id' => 'sidebar-footer-4',
        'description' => '',
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget' => '</aside>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ));
}

/**
 * Enqueue scripts
 */
add_action( 'wp_enqueue_scripts', 'tatada_enqueue_styles', 10 );

function tatada_enqueue_styles()
{
    global $theme_version;

    wp_enqueue_style( 'tatada-style', get_stylesheet_uri(), array() );
    wp_enqueue_style( 'custom-style', get_template_directory_uri() . '/css/custom.css', array() );
    wp_enqueue_style( 'font-awesome-style', get_template_directory_uri() . '/css/font-awesome.min.css', array() );
}

add_action( 'wp_enqueue_scripts', 'tatada_enqueue_scripts', 10 );

function tatada_enqueue_scripts()
{
    global $theme_version;

    wp_enqueue_script( 'bootstrap-js', get_template_directory_uri() . '/js/bootstrap.min.js', array( 'jquery' ), $theme_version, true );
    wp_enqueue_script( 'main-js', get_template_directory_uri() . '/js/main.js', array( 'jquery' ), $theme_version, true );
    wp_localize_script( 'main-js', 'ajax_object', array(
        'url' => admin_url( 'admin-ajax.php' ),
        'base_url' => home_url()
    ));
}

/*
 * Change archive title
 */
add_filter( 'get_the_archive_title', function ( $title )
{
    if ( is_category() ) {
        $title = single_cat_title( '', false );
    }
    elseif ( is_tag() ) {
        $title = single_tag_title( '', false );
    }
    elseif ( is_author() ) {
        $title = '<span class="vcard">' . get_the_author() . '</span>' ;
    }
    return $title;
});

/*
 * Get post view
 */
function tatada_set_post_views( $postID )
{
    $count_key = 'tatada_post_views_count';
    $count = get_post_meta( $postID, $count_key, true );
    if ( $count == '' ) {
        delete_post_meta( $postID, $count_key );
        add_post_meta( $postID, $count_key, '0' );
    }
    else {
        $count++;
        update_post_meta( $postID, $count_key, $count );
    }
}

//To keep the count accurate, lets get rid of prefetching
remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0 );

function tatada_get_post_views( $postID )
{
    $count_key = 'tatada_post_views_count';
    $count = get_post_meta( $postID, $count_key, true );
    if ( $count == '' ) {
        delete_post_meta( $postID, $count_key );
        add_post_meta( $postID, $count_key, '0' );
        return "0";
    }
    return $count;
}

add_filter( 'excerpt_more', 'tatada_excerpt_more' );
function tatada_excerpt_more( $more )
{
    return ' ...';
}