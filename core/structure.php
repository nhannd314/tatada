<?php
/**
 * Template functions used for the website
 */

/**
 * Display Logo
 */
if ( !function_exists( 'tatada_logo' ) )
{
    function tatada_logo()
    {
        ?>
        <div class="logo">

            <?php if (is_front_page()): ?>

                <h1 style="width: 150px; height: 100px; margin: 0 0 -100px 0; position: relative; top: -300px; font-size: 18px" class="site-title"><a href="<?php echo esc_url(home_url('/')); ?>" rel="home"><?php bloginfo('name'); ?></a></h1>

            <?php endif; ?>

            <a href="<?php echo home_url('/') ?>">
                <img src="<?php echo get_template_directory_uri() ?>/img/logo.png" alt="<?php bloginfo('name') ?>" />
            </a>
        </div><?php
    }
}

if ( !function_exists( 'tatada_nav' ) )
{
    function tatada_nav( $location )
    {
        $args = array(
            'theme_location' => $location,
            'depth' => 0,
            'container' => false,
            'menu_id' => 'main-menu'
        );

        wp_nav_menu($args);
    }
}

if ( !function_exists( 'tatada_mobile_nav' ) )
{
    function tatada_mobile_nav( $location )
    {
        $args = array(
            'theme_location' => $location,
            'depth' => 0,
            'container' => false,
            'menu_id' => 'mobile-nav'
        );

        wp_nav_menu($args);
    }
}

/**
 * Display post thumbnail
 */
if ( !function_exists( 'tatada_thumbnail' ) )
{
    function tatada_thumbnail( $size = 'thumbnail', $has_link = true, $args = array() )
    {
        if ( has_post_thumbnail() && !post_password_required() || has_post_format( 'image' ) ) {
            if ( $has_link ) {
                ?>
                <a class="post-thumbnail" href="<?php the_permalink() ?>" title="<?php the_title_attribute() ?>">
                    <figure><?php the_post_thumbnail( $size, $args ); ?></figure>
                </a><?php
            }
            else {
                ?>
                <figure class="post-thumbnail"><?php the_post_thumbnail( $size, $args ); ?></figure><?php
            }
        }
    }
}

/**
 * Display post entry meta
 */
if ( !function_exists( 'tatada_entry_meta' ) )
{
    function tatada_entry_meta()
    {
        ?>
        <div class="post-meta">
            <span class="author">Posted by <?php the_author_posts_link(); ?></span> | <span itemprop="datePublished"><?php the_time( 'd-m-Y' ) ?></span> | <span class="list-categories"><?php echo get_the_category_list( ', ' ) ?></span>
        </div><?php
    }
}

/**
 * Like share Facebook, Twitter
 */
if ( !function_exists( 'tatada_like_share' ) )
{
    function tatada_like_share()
    {
        $image = esc_url( wp_get_attachment_url( get_post_thumbnail_id() ) );
        $permalink = esc_url( apply_filters( 'the_permalink', get_permalink() ) );
        $title = esc_attr( get_the_title() );
        ?>
        <div class="like-share clearfix">
            <a href="http://www.facebook.com/sharer.php?m2w&amp;s=100&amp;p&#091;url&#093;=<?php echo $permalink ?>&amp;p&#091;images&#093;&#091;0&#093;=<?php echo $image ?>&amp;p&#091;title&#093;=<?php echo $title ?>" target="_blank" rel="nofollow" title="Facebook" class="share-facebook"><?php _e( 'Facebook', 'tatada' ) ?></a>
            <a href="https://twitter.com/intent/tweet?text=<?php echo $title ?>&amp;url=<?php echo $permalink ?>" target="_blank" rel="nofollow" title="Twitter" class="share-twitter"><?php _e( 'Twitter', 'tatada' ) ?></a>
            <a href="https://www.linkedin.com/shareArticle?mini=true&amp;url=<?php echo $permalink ?>&amp;title=<?php echo $title ?>" target="_blank" rel="nofollow" title="LinkedIn" class="share-linkedin"><?php _e( 'LinkedIn', 'tatada' ) ?></a>
            <a href="https://plus.google.com/share?url=<?php echo $permalink ?>" target="_blank" rel="nofollow" title="Google +" class="share-googleplus"><?php _e( 'Google Plus', 'tatada' ) ?></a>
            <a href="https://pinterest.com/pin/create/button/?url=<?php echo $permalink ?>&amp;media=<?php echo $image ?>" target="_blank" rel="nofollow" title="Pinterest" class="share-pinterest"><?php _e( 'Pinterest', 'tatada' ) ?></a>
            <a href="mailto:?subject=<?php echo $title ?>&amp;body=<?php echo $permalink ?>" target="_blank" rel="nofollow" title="Email" class="share-email"><?php _e( 'Email', 'tatada' ) ?></a>
        </div><?php
    }
}

/**
 * Comment Facebook
 */
if ( !function_exists( 'tatada_comment_facebook' ) )
{
    function tatada_comment_facebook( $link = '' )
    {
        if ( $link == '' ) $link = home_url( '/' );
        ?>
        <div class="facebook-comment responsive">
            <h4 class="title"><?php _e( 'Your comment', 'tatada' ) ?></h4>
            <div class="fb-comments" data-href="<?php echo $link ?>" data-width="817px" data-numposts="20"></div>
        </div><?php
    }
}

/**
 * Show Related Posts
 */

function tatada_get_related_posts( $ID = '', $post_type = 'post' )
{
    if ( $ID == '' || !is_numeric( $ID ) ) return array();
    $terms = wp_get_post_tags( $ID );

    if ( $terms ) {
        $term_ids = array();
        foreach ( $terms as $item ) $term_ids[] = $item->term_id;
        $args = array(
            'post_type' => $post_type,
            'tag__in' => $term_ids,
            'post__not_in' => array( $ID ),
            'posts_per_page' => 8,
            'ignore_sticky_posts' => 1
        );
        return new WP_Query( $args );
    }
    return array();
}

function tatada_related_posts( $ID, $post_type = 'post', $content_template = '' )
{
    $related_posts = tatada_get_related_posts( $ID, $post_type );

    if ( !empty ( $related_posts ) && $related_posts->have_posts() ) {
        while ( $related_posts->have_posts() ) {
            $related_posts->the_post();
            get_template_part( 'content', $content_template );
        }
        wp_reset_postdata();
    }
}

/**
 * @param $limit
 * @return array|mixed|string
 * Custom post excerpt
 */
function tatada_get_the_excerpt( $limit )
{
    $excerpt = explode( ' ', get_the_excerpt(), $limit );

    if ( count( $excerpt ) >= $limit ) {
        array_pop( $excerpt );
        $excerpt = implode( " ", $excerpt ) . ' ...';
    }
    else {
        $excerpt = implode( " ", $excerpt );
    }

    return $excerpt;
}

function tatada_the_excerpt($limit)
{
    echo tatada_get_the_excerpt($limit);
}

function tatada_pagination()
{
    global $wp_query;

    if ( $wp_query->max_num_pages > 1 ) : ?>

        <nav class="pagination">
            <ul>

                <?php
                if ( get_previous_posts_link() ) echo '<li class="pagination-newer">' . get_previous_posts_link( '&larr; ' . __( 'Previous', 'tatada' ) ) . '</li>';
                $paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;
                $max   = intval( $wp_query->max_num_pages );

                /**	Add current page to the array */
                if ( $paged >= 1 )
                    $links[] = $paged;

                /**	Add the pages around the current page to the array */
                if ( $paged >= 3 ) {
                    $links[] = $paged - 1;
                    $links[] = $paged - 2;
                }

                if ( ( $paged + 2 ) <= $max ) {
                    $links[] = $paged + 2;
                    $links[] = $paged + 1;
                }

                /**	Link to first page, plus ellipses if necessary */
                if ( ! in_array( 1, $links ) ) {
                    $class = 1 == $paged ? ' active' : '';

                    printf( '<li class="number%s"><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( 1 ) ), '1' );

                    if ( ! in_array( 2, $links ) )
                        echo '<li>...</li>';
                }

                /**	Link to current page, plus 2 pages in either direction if necessary */
                sort( $links );
                foreach ( (array) $links as $link ) {
                    $class = $paged == $link ? ' active' : '';
                    printf( '<li class="number%s"><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $link ) ), $link );
                }

                /**	Link to last page, plus ellipses if necessary */
                if ( ! in_array( $max, $links ) ) {
                    if ( ! in_array( $max - 1, $links ) )
                        echo '<li class="number">...</li>' . "\n";

                    $class = $paged == $max ? ' active' : '';
                    printf( '<li class="number%s"><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $max ) ), $max );
                }

                if ( get_next_posts_link() ) echo '<li class="pagination-older">' . get_next_posts_link( __( 'Next', 'tatada' ) . ' &rarr;' ) . '</li>';
                ?>

            </ul>
        </nav><!-- /pagination -->

    <?php endif;
}

function tatada_alert($text) {
    echo '<script>alert("'.$text.'")</script>';
}