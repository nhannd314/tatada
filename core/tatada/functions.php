<?php

require get_template_directory() . '/core/tatada/user.php';
require get_template_directory() . '/core/tatada/mail.php';
require get_template_directory() . '/core/tatada/create-website.php';

show_admin_bar(false);

/**
 * Ajax create website
 */
add_action( 'wp_ajax_prepare_create_website', 'tatada_prepare_create_website' );
add_action( 'wp_ajax_nopriv_prepare_create_website', 'tatada_prepare_create_website' );

function tatada_prepare_create_website()
{
    /**
     * Create user and Send active mail
     **/

    // First check the nonce, if it fails the function will break
    //check_ajax_referer( 'ajax-register-nonce', 'security' );

    $template_id = sanitize_text_field( $_POST['template_id'] );

    $new_user = array(
        'user_login' => sanitize_user( $_POST['shop_name'] ),
        'user_email' => sanitize_email( $_POST['email'] ),
        'user_pass' => sanitize_text_field( $_POST['password'] ),
        'first_name' => sanitize_text_field( $_POST['name'] )
    );

    // validate website user
    $errors = tatada_validate_user( $new_user );

    if ( ! empty( $errors ) ) {
        echo json_encode( array( 'status' => 'error', 'errors' => $errors ) );
        die();
    }

    $new_user_id = wp_insert_user( $new_user );

    // On success creating user
    if ( !is_wp_error( $new_user_id ) ) {
        // add user meta is_active = 0
        update_user_meta( $new_user_id, 'is_active', 0 );

        // add active key & active time to user meta then send mail to active
        $time = time();
        $key = md5( $new_user_id . $time . mt_rand( 1, 1000 ) );

        update_user_meta( $new_user_id, 'active_key', $key );
        update_user_meta( $new_user_id, 'active_time', $time );

        // send mail to active user
        // if send mail fail
        if ( !tatada_send_mail_active( $new_user_id, $new_user['user_email'], $key ) ) {
            echo json_encode( array( 'status' => 'error', 'errors' => array( 'Không thể gửi email. Vui lòng kiểm tra lại email của bạn hoặc thử dùng email khác!' ) ) );
            die();
        }

        // create website
        if ( tatada_create_sub_domain( $new_user, $template_id ) ) {
            echo json_encode( array( 'status' => 'success', 'sub_domain' => 'http://' . $new_user['user_login'] . '.tatada.vn/install.php' ) );
            die();
        }

        echo json_encode( array( 'status' => 'error', 'errors' => array( 'Quá trình tạo lập website bị lỗi, vui lòng thử lại sau hoặc liên hệ với chúng tôi!' ) ) );
        die();
    }

    echo json_encode( array( 'status' => 'error', 'errors' => array( 'Lỗi khi tạo người dùng mới, vui lòng thử lại sau!' ) ) );
    die();

}