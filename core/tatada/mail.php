<?php

function tatada_send_mail_active($user_id, $user_email, $key)
{
    return true;
    // get the posted data
    $email_address = 'wordpress@trithay.tv';

    // write the email content
    $header = "MIME-Version: 1.0\n";
    $header .= "Content-Type: text/html; charset=utf-8\n";
    $header .= "From:" . $email_address;

    $subject = "Đăng ký tài khoản thành công trên http://trithay.tv";
    $subject = "=?utf-8?B?" . base64_encode($subject) . "?=";
    $to = $user_email;

    $link = add_query_arg( array( 'key' => $key, 'user' => $user_id ), home_url('/active-user') );
    $message = 'Chúc mừng bạn đã đăng ký thành công tài khoản trên trithay.tv. Để kích hoạt tài khoản vui lòng click vào đường link sau để xác nhận: ' . $link;

    // send the email using wp_mail()
    return wp_mail($to, $subject, $message, $header);
}