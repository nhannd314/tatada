<?php

function tatada_validate_user( $user_array )
{
    $errors = array();

    $username = $user_array['user_login'];
    $password = $user_array['user_pass'];
    $email    = $user_array['user_email'];

    if ( 4 > strlen( $username ) ) {
        array_push( $error, 'Username quá ngắn. Cần ít nhất là 4 ký tự!' );
    }
    if ( username_exists( $username ) ) {
        array_push( $error, 'Username này đã tồn tại!' );
    }
    if ( ! validate_username( $username ) ) {
        array_push( $error, 'Username không hợp lệ!' );
    }
    if ( 5 > strlen( $password ) ) {
        array_push( $error, 'Mật khẩu phải lớn hơn 5 ký tự!' );
    }
    if ( !is_email( $email ) ) {
        array_push($error, 'Email không hợp lệ!' );
    }
    if ( email_exists( $email ) ) {
        array_push($error, 'Email này đã tồn tại!' );
    }

    return $errors;
}

function tatada_user_id_exists( $user ) {

    global $wpdb;

    $count = $wpdb->get_var($wpdb->prepare("SELECT COUNT(*) FROM $wpdb->users WHERE ID = %d", $user));

    if($count == 1) { return true; } else { return false; }

}

// active user page
//add_action( 'template_redirect', 'tatada_activate_user_page' );
function tatada_activate_user_page()
{
    $parttern = '/(\/active-user\?key\=.*)/';
    if ( preg_match($parttern, $_SERVER['REQUEST_URI']) )
    {
        $user_id = filter_input( INPUT_GET, 'user', FILTER_VALIDATE_INT, array( 'options' => array( 'min_range' => 1 ) ) );

        // if user id not exist
        if (!tatada_user_id_exists($user_id)) {
            tatada_alert( 'Username không tồn tại!' );
        }
        else {
            // if is active before
            if (get_user_meta($user_id, 'is_active', TRUE) == '1') {
                tatada_alert( 'Tài khoản đã được kích hoạt trước đó!');
            }
            else {
                // check expired time
                $time = get_user_meta($user_id, 'active_time', TRUE);
                // if more than 2 days (48h)
                if (time() - $time > 172800) {
                    // invalid, delete user then re register
                    wp_delete_user($user_id);
                    tatada_alert('Mã kích hoạt hết hạn, vui lòng đăng ký lại!');
                }
                else {
                    $key = get_user_meta($user_id, 'active_key', TRUE);

                    // delete user meta active_key, active_time and then active user
                    if ($key == filter_input( INPUT_GET, 'key' )) {
                        delete_user_meta($user_id, 'active_key');
                        delete_user_meta($user_id, 'active_time');
                        update_user_meta($user_id, 'is_active', 1);
                        tatada_alert( 'Kích hoạt thành công! Đăng nhập tài khoản để sử dụng ngay!' );
                    }
                    else {
                        tatada_alert( 'Mã kích hoạt không đúng! Vui lòng kiểm tra lại!' );
                    }
                }
            }
        }

        echo '<script>window.location = "' . home_url('/') . '";</script>';
    }
}

/**
 * Add Custom User Meta
 */

add_action( 'show_user_profile', 'tatada_show_user_meta' );
add_action( 'edit_user_profile', 'tatada_show_user_meta' );

function tatada_show_user_meta( $user )
{
    ?>
    <h3>Thông tin bổ sung</h3>
    <table class="form-table">
        <tr>
            <th>Kích hoạt tài khoản</th>
            <td>
                <label for="is_active">
                    <input type="checkbox" name="is_active" id="is_active" value="1"<?php echo get_the_author_meta( 'is_active', $user->ID ) == '1' ? ' checked' : '' ?>>
                    Tick chọn nếu đã kích hoạt tài khoản
                </label>
            </td>
        </tr>
        <tr>
            <th><label for="expired_date">Ngày hết hạn</label></th>
            <td>
                <input type="text" name="expired_date" id="expired_date" class="regular-text" value="<?php echo get_the_author_meta( 'expired_date', $user->ID ) ?>"><br />
                <span class="description">Ngày hết hạn của tài khoản</span>
            </td>
        </tr>
    </table>
<?php
}

add_action( 'personal_options_update', 'tatada_save_user_meta' );
add_action( 'edit_user_profile_update', 'tatada_save_user_meta' );

function tatada_save_user_meta( $user_id ) {

    if ( !current_user_can( 'edit_user', $user_id ) ) return false;

    update_user_meta( $user_id, 'is_active', ( isset( $_POST['is_active'] ) && $_POST['is_active'] == '1' ) ? 1 : 0 );
    update_user_meta( $user_id, 'membership', ( isset( $_POST['membership'] ) && $_POST['membership'] == '1' ) ? 1 : 0 );
    update_user_meta( $user_id, 'expired_date', $_POST['expired_date'] );
}

// add date picker to admin
add_action( 'admin_enqueue_scripts', 'tatada_enqueue_date_picker' );

function tatada_enqueue_date_picker()
{
    wp_enqueue_script(
        'field-date-js',
        get_template_directory_uri().'/js/admin.js',
        array('jquery', 'jquery-ui-core', 'jquery-ui-datepicker'),
        time(),
        true
    );
    wp_enqueue_style('jquery-style', 'http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.2/themes/smoothness/jquery-ui.css');
}