<?php

/**
 * Register widgets
 */
add_action( 'widgets_init', 'tatada_register_widgets' );

function tatada_register_widgets()
{
    register_widget( 'Tatada_Recent_Posts_Widget' );
    register_widget( 'Tatada_Most_View_Widget' );
}

/**
 * Class Tatada_Recent_Posts_Widget
 */

if ( !class_exists('Tatada_Recent_Posts_Widget' ) )
{
    class Tatada_Recent_Posts_Widget extends WP_Widget {

        function __construct() {
            /* Widget settings. */
            $widget_ops = array( 'classname' => 'tatada-recent-posts-widget', 'description' => 'Tatada Recent Posts Widget, recent posts with thumbnail' );
            /* Widget control settings. */
            $control_ops = array( 'id_base' => 'tatada-recent-posts-widget' );
            /* Create the widget. */
            parent::__construct( 'tatada-recent-posts-widget', 'Tatada Recent Posts Widget', $widget_ops, $control_ops );
        }

        function form( $instance ) {

            $default = array(
                'title' => __('Recent Posts', 'tatada'),
                'post_type' => 'post',
                'post_number' => 5,
                'category_id' => ''
            );
            $instance = wp_parse_args( (array) $instance, $default );
            $title = esc_attr($instance['title']);
            $post_type = esc_attr($instance['post_type']);
            $post_number = esc_attr($instance['post_number']);
            $category_id = esc_attr($instance['category_id']);

            echo '<p>'.__('Widget title', 'tatada').' <input type="text" class="widefat" name="'.$this->get_field_name('title').'" value="'.$title.'" /></p>';
            echo '<p>'.__('Post type', 'tatada').' <input type="text" class="widefat" name="'.$this->get_field_name('post_type').'" value="'.$post_type.'" /></p>';
            echo '<p>'.__('Number of posts', 'tatada').' <input type="text" class="widefat" name="'.$this->get_field_name('post_number').'" value="'.$post_number.'" /></p>';
            echo '<p>'.__('Category id', 'tatada').' <input type="text" class="widefat" name="'.$this->get_field_name('category_id').'" value="'.$category_id.'" /></p>';

        }

        function update( $new_instance, $old_instance ) {
            $instance = $old_instance;

            $instance['title'] = strip_tags($new_instance['title']);
            $instance['post_type'] = strip_tags($new_instance['post_type']);
            $instance['post_number'] = strip_tags($new_instance['post_number']);
            $instance['category_id'] = strip_tags($new_instance['category_id']);

            return $instance;
        }

        function widget( $args, $instance ) {
            extract($args);
            $title = apply_filters( 'widget_title', $instance['title'] );
            $post_type = $instance['post_type'];
            $post_number = $instance['post_number'];
            $category_id = $instance['category_id'];

            echo $before_widget; ?>

            <h3 class="widget-title"><span><?= $title ?></span></h3>

            <?php
            $args = array(
                'post_type' => $post_type,
                'posts_per_page' => $post_number
            );

            if ($category_id != '') $args['cat'] = $category_id;
            query_posts( $args );
            ?>

            <ul>

                <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

                    <li class="clearfix">
                        <?php tatada_thumbnail( 'thumbnail' ) ?>
                        <h3 class="post-title"><a href="<?php the_permalink() ?>"><?php the_title() ?></a></h3>
                    </li>

                <?php endwhile; endif; ?>

            </ul>

            <?php
            wp_reset_query();

            echo $after_widget;
        }
    }
    // end class
}

/**
 * Class Tatada_Most_View_Widget
 */

if ( !class_exists( 'Tatada_Most_View_Widget' ) )
{
    class Tatada_Most_View_Widget extends WP_Widget {

        function __construct() {
            /* Widget settings. */
            $widget_ops = array( 'classname' => 'tatada-most-view-widget', 'description' => 'Tatada Most View Widget' );
            /* Widget control settings. */
            $control_ops = array( 'id_base' => 'tatada-most-view-widget' );
            /* Create the widget. */
            parent::__construct( 'tatada-most-view-widget', 'Tatada Most View Widget', $widget_ops, $control_ops );
        }

        function form( $instance ) {

            $default = array(
                'title' => __( 'Most View', 'tatada' ),
                'post_number' => 5
            );
            $instance = wp_parse_args( (array)$instance, $default );
            $title = esc_attr( $instance['title'] );
            $post_number = esc_attr( $instance['post_number'] );

            echo '<p>'.__('Widget title', 'tatada').' <input type="text" class="widefat" name="'.$this->get_field_name('title').'" value="'.$title.'" /></p>';
            echo '<p>'.__('Post number', 'tatada').' <input type="text" class="widefat" name="'.$this->get_field_name('post_number').'" value="'.$post_number.'" /></p>';
        }

        function update( $new_instance, $old_instance ) {
            $instance = $old_instance;

            $instance['title'] = strip_tags($new_instance['title']);
            $instance['post_number'] = strip_tags($new_instance['post_number']);

            return $instance;
        }

        function widget( $args, $instance ) {
            extract($args);
            $title = apply_filters( 'widget_title', $instance['title'] );
            $post_number = $instance['post_number'];

            echo $before_widget; ?>

            <h3 class="widget-title"><span><?= $title ?></span></h3>
            <?php
            $args = array(
                'post_type' => 'post',
                'posts_per_page' => $post_number,
                'meta_key' => 'tatada_post_views_count',
                'orderby' => 'meta_value_num',
                'order' => 'DESC'
            );
            query_posts( $args );
            ?>

            <ul>

                <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

                    <li class="clearfix">
                        <?php tatada_thumbnail( 'thumbnail' ) ?>
                        <h3 class="post-title"><a href="<?php the_permalink() ?>"><?php the_title() ?></a></h3>
                    </li>

                <?php endwhile; endif; ?>

            </ul>

            <?php
            wp_reset_query();

            echo $after_widget;
        }
    }
    // end class
}