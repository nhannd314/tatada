<?php
/**
 * The template for displaying the footer.
 */
?>

<div id="bottom-action">
    <div class="container text-center">
        <h4 class="title">Hơn 500 khách hàng đang sử dụng rất hiệu quả, bạn có muốn thử không?</h4>
        <h4 class="sub-title">Hãy đăng ký dùng thử TaTada 15 ngày để cảm nhận</h4>
        <form>
            <input type="text" name="shop-name" placeholder="Nhập tên cửa hàng của bạn" id="bottom-create-website-shop-name-input">
            <button type="button" data-toggle="modal" data-target="#create-website-modal" id="bottom-create-website-button" data-shopname="">Tạo website ngay</button>
        </form>
    </div>
</div>

<footer id="footer">
    <div class="container footer">
        <div class="row">
            <div class="col-md-4 col-sm-6 col-xs-6">

                <?php dynamic_sidebar( 'sidebar-footer-1' ) ?>

            </div>
            <div class="col-md-2 col-sm-6 col-xs-6">

                <?php dynamic_sidebar( 'sidebar-footer-2' ) ?>

            </div>
            <div class="col-md-2 col-sm-6 col-xs-6">

                <?php dynamic_sidebar( 'sidebar-footer-3' ) ?>

            </div>
            <div class="col-md-4 col-sm-6 col-xs-6">

                <?php dynamic_sidebar( 'sidebar-footer-4' ) ?>

            </div>
        </div>
    </div>
</footer>
<div id="copyright">
    <div class="container">
        <div class="pull-left">© 2015 Tatada.vn. All Rights Reserved.</div>
        <div class="pull-right"></div>
    </div>
</div>

<div id="up-button">
    <i class="fa fa-angle-up"></i>
</div>

<!-- create website modal -->
<div class="modal fade" id="create-website-modal" tabindex="-1" role="dialog" aria-labelledby="create-website-modal-label">
    <div class="modal-dialog" role="document">
        <div id="create-website-modal-hover"></div>
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <div class="modal-header">
                <h3 class="modal-title text-center" id="create-website-modal-label">Tạo website bán hàng tự động với TaTada</h3>
                <h4 class="modal-sub-title text-center">Sử dụng miễn phí 15 ngày với đầy đủ tính năng</h4>
            </div>
            <div class="modal-body">
                <form id="create-website-form">
                    <p id="create-website-message"></p>
                    <select id="create-website-template-id" class="form-control">
                        <option value="0">Vui lòng chọn mẫu giao diện</option>

                        <?php
                        $templates = new WP_Query( array(
                            'post_type' => 'template',
                            'posts_per_page' => -1,
                            'post_status' => 'published'
                        ) );

                        // The Loop
                        if ( $templates->have_posts() ) {
                            while ( $templates->have_posts() ) {
                                $templates->the_post();
                                echo '<option value="' . get_the_ID() . '">#' . get_the_ID(). ': ' . get_the_title() . '</option>';
                            }
                            /* Restore original Post Data */
                            wp_reset_postdata();
                        }
                        ?>

                    </select>
                    <p class="help">Xem chi tiết kho giao diện <a href="<?php echo home_url() ?>/kho-giao-dien/" target="_blank">tại đây</a></p>
                    <input id="create-website-shop-name" class="form-control" type="text" placeholder="Tên cửa hàng của bạn" required>
                    <p class="help"><span style="color: orangered">.tatada.vn</span>. Nhập tên miền không dấu, viết liền, có thể sử dụng dấu -</p>
                    <input id="create-website-name" class="form-control" type="text" placeholder="Họ tên của bạn" required>
                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-xs-6"><input id="create-website-email" class="form-control" type="email" placeholder="Email của bạn" required></div>
                        <div class="col-md-6 col-sm-6 col-xs-6"><input id="create-website-phone" class="form-control" type="number" placeholder="Số điện thoại của bạn" required></div>
                    </div>
                    <input id="create-website-password" class="form-control" type="password" placeholder="Mật khẩu của bạn" required>
                    <input id="create-website-password-again" class="form-control" type="password" placeholder="Nhập lại mật khẩu" required>
                    <p id="policy-tick" class="text-center" data-ticked="0"><i class="fa fa-check-square-o"></i> Tôi đồng ý với <a href="#" target="_blank">Quy định sử dụng</a> và <a href="#" target="_blank">Chính sách bảo mật</a> của TaTada</p>
                    <div class="text-center"><button id="create-website-submit" type="submit" class="btn btn-primary">Tạo website của tôi</button></div>
                </form>
            </div>
        </div>
    </div>
</div>
<!--/ create website modal -->

<!-- facebook sdk -->
<div id="fb-root"></div>
<script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.6&appId=532832486868226";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>

<?php wp_footer(); ?>

</body>
</html>
