<?php

/**
 * Setup:
 * Load theme text domain
 * Register nav, widget
 * Register custom post type
 * Enqueue styles, etc.
 */

require get_template_directory() . '/core/setup.php';

/**
 * Structure
 */
require get_template_directory() . '/core/structure.php';

/**
 * Widget
 */
require get_template_directory() . '/core/widgets.php';

/**
 * Logic
 */
require get_template_directory() . '/core/tatada/functions.php';