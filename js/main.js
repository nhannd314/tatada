jQuery(document).ready(function()
{
    // add fix top navabar
    jQuery(window).on('scroll', function () {
        var scrollTop = jQuery(window).scrollTop();
        if (scrollTop > 100) {
            jQuery("#up-button").stop().fadeIn(300);
            //jQuery("#main-nav").addClass("navbar-fixed-top");
        }
        else {
            jQuery("#up-button").stop().fadeOut(300);
            //jQuery("#main-nav").removeClass("navbar-fixed-top");
        }
    });

    // up to top
    jQuery("#up-button").click(function () {
        jQuery("html, body").animate({scrollTop: 0}, 500);
    });

    jQuery("#main-nav .navbar-nav > .menu-item-has-children").hover(function() {
        jQuery(this).find("ul.dropdown-menu").stop().slideDown(300);
    }, function() {
        jQuery(this).find("ul.dropdown-menu").stop().slideUp(300);
    });

    // create website
    jQuery("#policy-tick").click(function() {
        var ticked = jQuery(this).data("ticked");
        if ( ticked == "0" ) {
            jQuery(this).addClass("ticked");
            jQuery(this).data("ticked", "1");
        }
        else {
            jQuery(this).removeClass("ticked");
            jQuery(this).data("ticked", "0");
        }
    });

    jQuery("#bottom-create-website-shop-name-input").change(function() {
        var shop_name = jQuery(this).val();
        jQuery("#bottom-create-website-button").data("shopname", shop_name);
    });
    // on click create website in template
    jQuery('#create-website-modal').on('show.bs.modal', function (event) {
        var button = jQuery(event.relatedTarget);
        var template_id = button.data('id');

        if ( template_id !== undefined) {
            jQuery('#create-website-template-id').val(template_id).trigger('change');
        }
        else {
            jQuery('#create-website-template-id').val(0).trigger('change');

            var shop_name = button.data("shopname");
            if (shop_name !== undefined) {
                jQuery("#create-website-shop-name").val(shop_name);
            }
            else {
                jQuery("#create-website-shop-name").val("");
            }
        }
    })

    // on submit create website form
    jQuery("#create-website-form").submit(function(evt) {

        var template_id = jQuery("#create-website-template-id").val();
        var shop_name = jQuery("#create-website-shop-name").val();
        var name = jQuery("#create-website-name").val();
        var email = jQuery("#create-website-email").val();
        var phone = jQuery("#create-website-phone").val();
        var password = jQuery("#create-website-password").val();
        var password_again = jQuery("#create-website-password-again").val();

        if (template_id == 0) {
            jQuery("#create-website-message").html("Vui lòng chọn một mẫu giao diện!");
            return false;
        }
        // check valid shop name
        if ( !valid_shop_name( shop_name ) ) {
            jQuery("#create-website-message").html("Bạn đặt tên cửa hàng chưa đúng quy cách!");
            return false;
        }

        // check password match
        if ( password != password_again ) {
            jQuery("#create-website-message").html("Password không khớp, vui lòng nhập lại password!");
            return false;
        }

        // check if tick policy
        if (jQuery("#policy-tick").data("ticked") == "0") {
            jQuery("#create-website-message").html("Bạn chưa đồng ý với điều khoản dịch vụ của TaTada");
            return false;
        }

        // check if shop name exist
        if ( shop_name_exist( shop_name ) ) {
            jQuery("#create-website-message").html("Tên cửa hàng đã tồn tại, vui lòng chọn tên khác!");
            return false;
        }

        jQuery("#create-website-modal-hover").show().html( '<div class="spinner"><div class="dot1"></div><div class="dot2"></div></div>' );

        // ajax send to server to create website
        var create_website_data = {
            'action': 'prepare_create_website',
            'template_id': template_id,
            'shop_name': shop_name,
            'name': name,
            'email': email,
            'phone': phone,
            'password': password
        };

        jQuery.ajax({
            type: "POST",
            dataType: "json",
            url: ajax_object.url,
            data: create_website_data,
            success: function(response){
                console.log(response);
                //jQuery("#create-website-message").html(response);
                jQuery("#create-website-modal-hover").html("").hide();
                // if has some errors
                if (response.status == 'error' ) {
                    // message errors to user
                    jQuery("#create-website-message").html(response.errors);
                }
                else if (response.status == 'success' ) {
                    //console.log('website created');
                    //window.location = response.sub_domain;
                }
            }
        });

        evt.preventDefault();
    });

});

function valid_shop_name( name )
{
    return true;
}

function shop_name_exist()
{
    return false;
}