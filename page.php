<?php
/**
 * The template for displaying all pages.
 */

get_header(); ?>

<main class="main main-single main-page">
    <div class="blog-header">
        <div class="container">

            <?php if ( function_exists('yoast_breadcrumb') )
            {yoast_breadcrumb('<div id="breadcrumbs">','</div>');} ?>

        </div>
    </div>
	<div class="container">
		<div class="row">
			<section class="content col-md-9 col-sm-8 col-xs-12" role="main">

				<?php
				if (have_posts()):

					while (have_posts()):
						the_post();
						get_template_part('content', 'page');
					endwhile;
					tatada_pagination();
				else:
					get_template_part('content', 'none');
				endif;
				?>

			</section>
			<aside class="sidebar col-md-3 col-sm-4 col-xs-12" role="complementary">

				<?php get_sidebar() ?>

			</aside>
		</div>
	</div>
</main><!--/ main -->

<?php get_footer(); ?>