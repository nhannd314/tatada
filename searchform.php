<form class="search-form" method="get" action="<?php echo home_url('/') ?>">
    <input type="text" name="s" placeholder="Tìm kiếm trong trang web" class="form-control">
    <button class="btn btn-success" type="submit"><i class="fa fa-search"></i></button>
</form>