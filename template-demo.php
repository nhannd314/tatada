<?php
/**
 * The template for displaying template demo.
 * Template name: Demo
 */

get_header();
$template_id = $_GET['template_id'];
$template_post = get_post( $template_id );
$demo_link = get_post_meta( $template_id, 'link_demo', true );
?>

<?php if (have_posts()) while(have_posts()):
    the_post(); ?>

    <main class="main main-demo">
        <nav id="demo-nav">
            <div class="container">
                <div class="pull-left logo">
                    <a href="<?php echo home_url( '/' ) ?>"><img src="<?php echo get_template_directory_uri() ?>/img/logo-dark.png" alt="Logo"></a>
                </div>
                <div class="pull-right">
                    <a href="#" class="create-website">Sử dụng giao diện này</a>
                </div>
            </div>
        </nav>
        <div id="demo-container" style="height: calc(100% - 70px);">
            <div id="demo-section">
                <iframe src="<?php echo $demo_link ?>"></iframe>
            </div>
        </div>
    </main>

<?php endwhile; ?>

<?php get_footer(); ?>