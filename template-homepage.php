<?php
/**
 * The template for displaying the homepage.
 * Template name: Homepage
 */

get_header();
?>

<?php if (have_posts()) while(have_posts()):
    the_post(); ?>

    <main class="main main-home">
        <div id="main-slider" class="text-center gap">
            <div class="hover"></div>
            <div class="container">
                <div class="text-wrap">
                    <h3 class="title">Tạo Website bán hàng tự động<br>Đầy đủ tính năng, hỗ trợ SEO tuyệt vời</h3>
                    <h4 class="sub-title">Vận hành website với chi phí chỉ từ <span style="color: #7fc142; font-weight: normal">3000đ</span> / ngày</h4>
                    <a class="create-website" href="#" data-toggle="modal" data-target="#create-website-modal">Dùng thử 15 ngày</a>
                </div>
            </div>
        </div>
        <!--/ #main-slider -->
        <div id="main-featured" class="section gap">
            <div class="container">
                <h2 class="section-title title text-center"><strong><span style="color: #7fc142">Ta</span><span style="color: #08c">Tada</span></strong> có gì Đặc Biệt?</h2>
                <h4 class="section-sub-title title text-center">TaTada được trang bị đầy đủ tính năng của một website bán hàng điển hình,<br>tùy biến không giới hạn và hỗ trợ SEO tuyệt vời</h4>
                <div class="row section-content">
                    <div class="col-md-3 col-sm-6 col-xs-12 text-right">
                        <div class="item">
                            <h4 class="item-title title">Khởi Tạo Đơn Giản</h4>
                            <p>Bạn chỉ cần nhập một số thông tin liên quan đến website, Tatada sẽ tự động tạo website cho bạn trong vòng 1 phút</p>
                        </div>
                        <div class="item">
                            <h4 class="item-title title">Tùy biến không giới hạn</h4>
                            <p>Mọi thứ gần như có thể thay đổi được trong trang quản trị, từ hình ảnh, màu sắc, font chữ đến các thông tin khác</p>
                        </div>
                        <div class="item">
                            <h4 class="item-title title">Kéo thả tạo trang</h4>
                            <p>Trình quản lý cho phép bạn kéo thả các thành phần để tạo trang chủ, landing page một cách dễ dàng và linh hoạt</p>
                        </div>
                        <div class="item">
                            <h4 class="item-title title">Hỗ trợ bán hàng tối đa</h4>
                            <p>Tích hợp đầy đủ các tính năng giỏ hàng, thanh toán online, tạo mã KHUYẾN MẠI ... hỗ trợ tối đa công việc bán hàng của bạn</p>
                        </div>
                    </div>
                    <div class="col-md-6 hidden-sm hidden-xs text-center">
                        <img src="<?php echo get_template_directory_uri() ?>/img/main-featured.png" alt="Tạo website tự động Tatada" />
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12 text-left">
                        <div class="item">
                            <h4 class="item-title title">Hỗ trợ SEO tuyệt vời</h4>
                            <p>Tối ưu đường dẫn, hỗ trợ tạo site maps, robots tự động, tùy biến thẻ meta cho bài viết, hỗ trợ tối ưu SEO cho bài viết ...</p>
                        </div>
                        <div class="item">
                            <h4 class="item-title title">Kho giao diện siêu đẹp</h4>
                            <p>Kho giao diện siêu đẹp, thiết kế hiện đại theo xu hướng thế giới, hiển thị tốt trên mọi kích thước màn hình di động</p>
                        </div>
                        <div class="item">
                            <h4 class="item-title title">Phân quyền quản trị</h4>
                            <p>Cho phép phân quyền quản trị theo cấp độ: admin (quyền cao nhất), editor, author, shop manager và thành viên (subscribe)</p>
                        </div>
                        <div class="item">
                            <h4 class="item-title title">Bảo mật mạnh mẽ</h4>
                            <p>Website được cấu hình bảo mật và cài đặt các tool bảo mật mạnh mẽ giúp phát hiện và chống tấn công, spam bình luận ...</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--/ #main-featured -->
        <div id="main-action" class="action text-center gap">
            <div class="container">
                <h4 class="title pull-left">Sở hữu ngay website bán hàng online đầy đủ tính năng và mạnh mẽ</h4>
                <div class="pull-right"><a href="#" class="create-website" data-toggle="modal" data-target="#create-website-modal">Tạo Website Ngay</a></div>
            </div>
        </div>
        <div id="templates-store" class="gap section">
            <div class="container">
                <h3 class="section-title title text-center">Kho giao diện cực đẹp</h3>
                <h4 class="section-sub-title title text-center">Hơn 100 mẫu giao diện cực đẹp, hiển thị tốt trên mọi màn hình di động, tùy biến không giới hạn</h4>
                <div class="row section-content">

                    <?php
                    query_posts( array(
                        'post_type' => 'template',
                        'posts_per_page' => 9
                    ) );

                    if ( have_posts() ) while ( have_posts() ) {
                        the_post();
                        get_template_part( 'content', 'template' );
                    }

                    wp_reset_query();
                    ?>

                </div>
                <div class="text-center"><a class="more-templates" href="<?php echo home_url( '/kho-giao-dien/' ) ?>">Xem thêm Giao diện</a></div>
            </div>
        </div>
        <!--/ #templates-store -->
        <div id="seo-detail" class="section gap">
            <div class="container">
                <h3 class="section-title title text-center">Website của <strong><span style="color: #7fc142">Ta</span><span style="color: #08c">Tada</span></strong> hỗ trợ SEO như thế nào?</h3>
                <h4 class="section-sub-title title text-center">Chuẩn SEO là tiêu chí quan trọng của mọi website</h4>
                <div class="section-content">
                    <div class="row">
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <img src="<?php echo get_template_directory_uri() ?>/img/duong-dan-url-toi-uu.jpg" alt="Đường dẫn tối ưu" />
                            <h4 class="title">Đường dẫn tối ưu</h4>
                            <p>
                                Cấu trúc đường dẫn (URL) thân thiện nhất với SEO. Cho phép bạn thay đổi đường dẫn, tùy chỉnh kiểu đường dẫn hoặc thay đổi tiền tố cho danh mục
                            </p>
                        </div>
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <img src="<?php echo get_template_directory_uri() ?>/img/tuy-bien-the-seo.jpg" alt="Tùy biến các thẻ SEO" />
                            <h4 class="title">Tùy biến các thẻ SEO</h4>
                            <p>
                                Cho phép tùy chỉnh các thẻ SEO của website, cấu hình mẫu chung cho các thẻ. Có thể viết riêng thẻ SEO cho từng bài viết để ghi đè mặc định
                            </p>
                        </div>
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <img src="<?php echo get_template_directory_uri() ?>/img/cham-diem-seo-bai-viet.jpg" alt="Chấm điểm SEO bài viết" />
                            <h4 class="title">Chấm điểm SEO bài viết</h4>
                            <p>
                                Trình quản trị tích hợp tính năng chấm điểm chuẩn SEO cho nội dung bài viết, đưa ra các tiêu chí và gợi ý hỗ trợ bạn viết bài chuẩn SEO
                            </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <img src="<?php echo get_template_directory_uri() ?>/img/sitemaps-robots.jpg" alt="Site maps, robots.txt" />
                            <h4 class="title">Site maps, robots.txt</h4>
                            <p>
                                Hỗ trợ sinh tự động file site maps, robots.txt. Cho phép bạn tùy chỉnh file, chặn index các nội dung mà bạn không muốn hiển thị lên google
                            </p>
                        </div>
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <img src="<?php echo get_template_directory_uri() ?>/img/google-analytics-api.jpg" alt="Google Analytics API" />
                            <h4 class="title">Google Analytics API</h4>
                            <p>
                                Tích hợp Google Analytics API lên website cho phép bạn theo dõi thống kê truy cập ngay trên chính trang dashboard quản trị
                            </p>
                        </div>
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <img src="<?php echo get_template_directory_uri() ?>/img/ho-tro-seo-nhieu-hon-nua.jpg" alt="Và còn nhiều hơn thế nữa" />
                            <h4 class="title">Và còn nhiều hơn thế nữa</h4>
                            <p>
                                TaTada sẽ tiếp tục bổ sung và hoàn thiện các tính năng của mình hơn nữa, hãy chờ đón những trải nghiệm tuyệt vời tiếp theo
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--/ #seo-detail -->
        <div id="testimonial" class="section gap">
            <div class="container">
                <h3 class="section-title title text-center">Khách hàng nói gì về <strong><span style="color: #7fc142">Ta</span><span style="color: #08c">Tada</span></strong></h3>
                <h4 class="section-sub-title text-center">Sự hài lòng của khách hàng là động lực lớn cho TaTada hoàn thiện sản phẩm tốt hơn nữa</h4>
                <div class="section-content">

                    <?php
                    query_posts(array(
                        'post_type'			=> 'testimonial',
                        'posts_per_page'	=> -1
                    ));
                    global $wp_query;
                    $post_count = $wp_query->post_count;
                    ?>

                    <div id="testimonial-carousel" class="carousel slide" data-ride="carousel">
                        <!-- Indicators -->
                        <ol class="carousel-indicators">
                            <li data-target="#testimonial-carousel" data-slide-to="0" class="active"></li>
                            <?php for ($i = 1; $i < $post_count; $i++): ?>
                                <li data-target="#testimonial-carousel" data-slide-to="<?php echo $i ?>"></li>
                            <?php endfor; ?>
                        </ol>
                        <!-- Wrapper for slides -->
                        <div class="carousel-inner" role="listbox">

                            <?php if ( have_posts() ): the_post() ?>

                            <div class="item active">
                                <div class="row">
                                    <div class="col-md-6 col-xs-6 customer text-center">
                                        <div class="tell"><?php the_content() ?></div>
                                        <?php the_post_thumbnail( 'full', array( 'class' => 'avatar' ) ) ?>
                                        <div class="name"><?php the_title() ?></div>
                                        <div class="link"><a href="<?php the_field( 'link_website' ) ?>" rel="nofollow" target="_blank"><?php the_field( 'link_website' ) ?></a></div>
                                    </div>
                                    <div class="col-md-6 col-xs-6 website">
                                        <?php echo wp_get_attachment_image( get_field( 'project_image' ), 'full' ) ?>
                                    </div>
                                </div>
                            </div>

                            <?php endif; if ( have_posts() ): while ( have_posts() ): the_post(); ?>

                                <div class="item">
                                    <div class="row">
                                        <div class="col-md-6 col-xs-6 customer text-center">
                                            <div class="tell"><?php the_content() ?></div>
                                            <?php the_post_thumbnail( 'full', array( 'class' => 'avatar' ) ) ?>
                                            <div class="name"><?php the_title() ?></div>
                                            <div class="link"><a href="<?php the_field( 'link_website' ) ?>" rel="nofollow" target="_blank"><?php the_field( 'link_website' ) ?></a></div>
                                        </div>
                                        <div class="col-md-6 col-xs-6 website">
                                            <?php echo wp_get_attachment_image( get_field( 'project_image' ), 'full' ) ?>
                                        </div>
                                    </div>
                                </div>

                            <?php endwhile; endif; wp_reset_query(); ?>

                        </div>

                        <!-- Controls -->
                        <a class="left carousel-control" href="#testimonial-carousel" role="button" data-slide="prev">
                            <i class="fa fa-angle-left icon-prev" aria-hidden="true"></i>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="right carousel-control" href="#testimonial-carousel" role="button" data-slide="next">
                            <i class="fa fa-angle-right icon-next" aria-hidden="true"></i>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </main>

<?php endwhile; ?>

<?php get_footer(); ?>