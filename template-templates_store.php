<?php
/**
 * The template for displaying the templates store.
 * Template name: Templates Store
 */

get_header();
?>

<?php if (have_posts()) while(have_posts()):
    the_post(); ?>

    <main class="main main-home">
        <div class="blog-header">
            <div class="container">

                <?php if ( function_exists('yoast_breadcrumb') )
                {yoast_breadcrumb('<div id="breadcrumbs">','</div>');} ?>

            </div>
        </div>
        <div id="templates-store" class="gap section">
            <div class="container">
                <h3 class="section-title title text-center">Kho giao diện cực đẹp</h3>
                <h4 class="section-sub-title title text-center">Hơn 100 mẫu giao diện cực đẹp, hiển thị tốt trên mọi màn hình di động, tùy biến không giới hạn</h4>
                <div class="row section-content">

                    <?php
                    query_posts( array(
                        'post_type' => 'template',
                        'posts_per_page' => 15,
                        'paged' => get_query_var( 'paged' )
                    ) );

                    if ( have_posts() ) {
                        while ( have_posts() ) {
                            the_post();
                            get_template_part( 'content', 'template' );
                        }
                        tatada_pagination();
                    }

                    wp_reset_query();
                    ?>

                </div>
            </div>
        </div>
        <!--/ #templates-store -->

    </main>

<?php endwhile; ?>

<?php get_footer(); ?>